Что выведет программа? Объяснить вывод программы.

```go
package main

import (
	"fmt"
	"math/rand"
	"time"
)

func asChan(vs ...int) <-chan int {
	c := make(chan int)

	go func() {
		for _, v := range vs {
			c <- v
			time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
		}

		close(c)
	}()
	return c
}

func merge(a, b <-chan int) <-chan int {
	c := make(chan int)
	go func() {
		for {
			select {
			case v := <-a:
				c <- v
			case v := <-b:
				c <- v
			}
		}
	}()
	return c
}

func main() {

	a := asChan(1, 3, 5, 7)
	b := asChan(2, 4 ,6, 8)
	c := merge(a, b )
	for v := range c {
		fmt.Println(v)
	}
}
```

Ответ:
```
Вывод: заданные значение + бесконечный поток, состоящий из 0.

При чтении из канала возвращается 2 значения: передаваемое значение и bool значение открытости канала. 
После закрытия канала чтение из канала возвращает zero-value и false. 

В функции merge в бесконечном цикле происходит чтение из каналов a и b. После закрытия каналов a и b
возвращаемые значения будут 0(zero-value для int), false. Так как в коде не проверяется второе значение, то
будет вечная передача 0.
```
