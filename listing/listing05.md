Что выведет программа? Объяснить вывод программы.

```go
package main

type customError struct {
	msg string
}

func (e *customError) Error() string {
	return e.msg
}

func test() *customError {
	{
		// do something
	}
	return nil
}

func main() {
	var err error
	err = test()
	if err != nil {
		println("error")
		return
	}
	println("ok")
}
```

Ответ:
```
Вывод: error

Интерфейс это структура, имеющая 2 поля, хранящих ссылку на значение и ссылку на тип данных.
При nil значении, тип данных может быть не nil, поэтому nil-interface не всегда равен nil.

Так как в error это интерфейс, то в поле с типом данных будет &customError. 
Поэтому выражении err != nil будет true
```
