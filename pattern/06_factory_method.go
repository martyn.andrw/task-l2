package pattern

/*
	Реализовать паттерн «фабричный метод».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Factory_method_pattern
*/

// Animal provides interface for animals
type Animal interface {
	Greet() string
}

// Cat implements animal interface
type Cat struct {
}

// Greet is a function that implements a cat greeting
func (c Cat) Greet() string {
	return "Meow!"
}

// Dog implements animal interface
type Dog struct {
}

// Greet is a function that implements a dog greeting
func (d Dog) Greet() string {
	return "Woof!"
}

// Gopher implements animal interface
type Gopher struct {
}

// Greet is a function that implements a gopher greeting
func (g Gopher) Greet() string {
	return "Hello world!"
}

// Creater implements Factory
type Creater struct {
}

// Create is a factory method
func (c Creater) Create(animal string) Animal {
	switch animal {
	case "cat":
		return Cat{}
	case "dog":
		return Dog{}
	case "gopher":
		return Gopher{}
	}
	return nil
}
