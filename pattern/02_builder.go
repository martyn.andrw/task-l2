package pattern

/*
	Реализовать паттерн «строитель».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Builder_pattern
*/

import "strings"

// GameOfYear implements a struct for builder.
type GameOfYear struct {
	Name    string
	Genre   string
	Company string
}

// BuilderGOY implements a builder for struct GameOfYear.
type BuilderGOY struct {
	name    string
	genre   string
	company string
}

// GetString returns string containing all parameters.
func (g GameOfYear) GetString() string {
	var sb strings.Builder
	sb.WriteString(g.Name)
	sb.WriteString(" - ")
	sb.WriteString(g.Company)
	sb.WriteString(" - ")
	sb.WriteString(g.Genre)
	return sb.String()
}

// NewBuilder is a constructor for builder.
func NewBuilder() BuilderGOY {
	return BuilderGOY{}
}

// SetName is a setter for builder.name
func (b BuilderGOY) SetName(val string) BuilderGOY {
	b.name = val
	return b
}

// SetGenre is a setter for builder.name
func (b BuilderGOY) SetGenre(val string) BuilderGOY {
	b.genre = val
	return b
}

// SetCompany is a setter for builder.name
func (b BuilderGOY) SetCompany(val string) BuilderGOY {
	b.company = val
	return b
}

// Build returns GameOfYear.
func (b BuilderGOY) Build() GameOfYear {
	return GameOfYear{
		Name:    b.name,
		Genre:   b.genre,
		Company: b.company,
	}
}

// Director implements director for builders
type Director struct {
	builder BuilderGOY
}

// NewDirector is a constructor for director
func NewDirector(b BuilderGOY) *Director {
	return &Director{
		builder: b,
	}
}

// SetBuilder is a setter for builder in director
func (d *Director) SetBuilder(b BuilderGOY) {
	d.builder = b
}

// BuildGOY builds GameOfYear by using director
func (d *Director) BuildGOY() GameOfYear {
	return d.builder.Build()
}
