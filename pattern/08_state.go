package pattern

/*
	Реализовать паттерн «состояние».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/State_pattern
*/

// NotifierStater provides state interface
type NotifierStater interface {
	Notify() string
}

// WebAlert implements an alert depending on its state
type WebAlert struct {
	state NotifierStater
}

// Notify executes state notify
func (w *WebAlert) Notify() string {
	return w.state.Notify()
}

// SetState is a setter for WebAlert.state
func (w *WebAlert) SetState(state NotifierStater) {
	w.state = state
}

// NotifyEn implements NotifierStater
type NotifyEn struct {
}

// Notify notifies
func (n *NotifyEn) Notify() string {
	return "Something went wrong"
}

// NotifyRu implements NotifierStater
type NotifyRu struct {
}

// Notify notifies
func (n *NotifyRu) Notify() string {
	return "Что-то пошло не так"
}

// NotifyDe implements NotifierStater
type NotifyDe struct {
}

// Notify notifies
func (n *NotifyDe) Notify() string {
	return "Etwas ist schief gelaufen"
}
