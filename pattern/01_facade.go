package pattern

/*
	Реализовать паттерн «фасад».
Объяснить применимость паттерна, его плюсы и минусы,а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Facade_pattern
*/

import (
	"strings"
)

// Gopher01 implements gopher and facade.
type Gopher01 struct {
	territory   *Territory
	burrow      *Burrow
	ooplanguage *OopLanguage
}

// Territory implements a subsystem "Territory"
type Territory struct {
}

// Burrow implements a subsystem "Burrow"
type Burrow struct {
}

// OopLanguage implements a subsystem "OopLanguage"
type OopLanguage struct {
}

// NewGopher01 creates gopher.
func NewGopher01() *Gopher01 {
	return &Gopher01{
		territory:   &Territory{},
		burrow:      &Burrow{},
		ooplanguage: &OopLanguage{},
	}
}

// DoYourTasks returns that average gopher must do.
func (g *Gopher01) DoYourTasks() string {
	var sb strings.Builder
	sb.WriteString(g.territory.Defend())
	sb.WriteString(g.burrow.Dig())
	sb.WriteString(g.ooplanguage.Create())
	return sb.String()
}

// Defend implementation.
func (h *Territory) Defend() string {
	return "Aggressively defends territories.\n"
}

// Dig implementation.
func (t *Burrow) Dig() string {
	return "Creates a burrow.\n"
}

// Create implementation.
func (c *OopLanguage) Create() string {
	return "Creates Golang.\n"
}
