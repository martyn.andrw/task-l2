package pattern

/*
	Реализовать паттерн «цепочка вызовов».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Chain-of-responsibility_pattern
*/

const (
	user = iota
	admin
	owner
)

// File provides file interface
type File interface {
	GetAccess(r int) string
}

// FileA implements file interface
type FileA struct {
	Next File
}

// GetAccess checks if user has access
func (u FileA) GetAccess(r int) string {
	if r == owner {
		return "Owner has access to the fileA"
	}
	if u.Next != nil {
		return u.Next.GetAccess(r)
	}
	return "No one has access to the fileA"
}

// FileB implements file interface
type FileB struct {
	Next File
}

// GetAccess checks if user has access
func (u FileB) GetAccess(r int) string {
	if r == admin {
		return "Admin has access to the fileB"
	}
	if u.Next != nil {
		return u.Next.GetAccess(r)
	}
	return "No one has access to the fileB"
}

// FileC implements file interface
type FileC struct {
	Next File
}

// GetAccess checks if user has access
func (u FileC) GetAccess(r int) string {
	if r == user {
		return "User has access to the fileC"
	}
	if u.Next != nil {
		return u.Next.GetAccess(r)
	}
	return "No one has access to the fileC"
}
