package pattern

/*
	Реализовать паттерн «комманда».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Command_pattern
*/

import "fmt"

// Command provides a command interface.
type Command interface {
	Execute()
}

// Device provides a device interface
type Device interface {
	On()
	Off()
}

// OffCommand implements Command interface
type OffCommand struct {
	device Device
}

// Execute executes device.off
func (o *OffCommand) Execute() {
	o.device.Off()
}

// OnCommand implements Command interface
type OnCommand struct {
	device Device
}

// Execute executes device.on
func (o *OnCommand) Execute() {
	o.device.On()
}

// Computer implements Device interface
type Computer struct {
	isRunning bool
}

// On changes isRunning to true
func (c *Computer) On() {
	if c.isRunning {
		fmt.Println("Computer already is running")
	} else {
		c.isRunning = true
		fmt.Println("Computer is running")
	}
}

// Off changes isRunning to false
func (c *Computer) Off() {
	if !c.isRunning {
		fmt.Println("Computer is already off")
	} else {
		c.isRunning = false
		fmt.Println("Computer is off")
	}
}

// IsRunning returns flag isRunning
func (c *Computer) IsRunning() bool {
	return c.isRunning
}

// Invoker is a slice of command
type Invoker struct {
	commands []Command
}

// Insert adds new command to Invoker
func (i *Invoker) Insert(c Command) {
	i.commands = append(i.commands, c)
}

// ExecuteAll executes all command from Invoker
func (i *Invoker) ExecuteAll() {
	for _, c := range i.commands {
		c.Execute()
	}
}
