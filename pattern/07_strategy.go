package pattern

/*
	Реализовать паттерн «стратегия».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Strategy_pattern
*/

// StrategySearch provides an interface for "search"
type StrategySearch interface {
	Search(string) string
}

// GoogleSearch implements StrategySearch
type GoogleSearch struct {
}

// Search is a function that returns string
func (s *GoogleSearch) Search(problem string) string {
	return "Google found solution for " + problem
}

// BingSearch implements StrategySearch
type BingSearch struct {
}

// Search is a function that returns string
func (s *BingSearch) Search(problem string) string {
	return "Bing found solution for " + problem
}

// DuckDuckGoSearch implements StrategySearch
type DuckDuckGoSearch struct {
}

// Search is a function that returns string
func (s *DuckDuckGoSearch) Search(problem string) string {
	return "DuckDuckGo found solution for " + problem
}

// Context provides a context for execution of a strategy.
type Context struct {
	strategy StrategySearch
}

// SetStrategy is a setter for Context.strategy
func (c *Context) SetStrategy(s StrategySearch) {
	c.strategy = s
}

// Search executes strategy search
func (c *Context) Search(problem string) string {
	return c.strategy.Search(problem)
}
