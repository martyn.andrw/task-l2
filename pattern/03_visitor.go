package pattern

/*
	Реализовать паттерн «посетитель».
Объяснить применимость паттерна, его плюсы и минусы, а также реальные примеры использования данного примера на практике.
	https://en.wikipedia.org/wiki/Visitor_pattern
*/

import "strings"

// Visitor provides interface for visitor
type Visitor interface {
	VisitStore(p *Store) string
	VisitCinema(p *Cinema) string
	VisitTheater(p *Theater) string
}

// Place provides an interface for place for visiting
type Place interface {
	Accept(Visitor) string
}

// Human implements typical visitor
type Human struct {
	name string
}

// VisitStore is a function when human visits a store
func (h Human) VisitStore(p *Store) string {
	var sb strings.Builder
	sb.WriteString(h.name)
	sb.WriteString(" ")
	sb.WriteString(p.BuySmt())
	return sb.String()
}

// VisitCinema is a function when human visits a cinema
func (h Human) VisitCinema(p *Cinema) string {
	var sb strings.Builder
	sb.WriteString(h.name)
	sb.WriteString(" ")
	sb.WriteString(p.WatchFilm())
	return sb.String()
}

// VisitTheater is a function when human visits a theater
func (h Human) VisitTheater(p *Theater) string {
	var sb strings.Builder
	sb.WriteString(h.name)
	sb.WriteString(" ")
	sb.WriteString(p.WatchPlay())
	return sb.String()
}

// City implements city
// there are places to visit in the city
type City struct {
	places []Place
}

// Add is function that adds place
func (c *City) Add(p Place) {
	c.places = append(c.places, p)
}

// Accept implements a visit to all places in the city.
func (c *City) Accept(v Visitor) string {
	var result strings.Builder

	for _, p := range c.places {
		result.WriteString(p.Accept(v))
		result.WriteString("\n")
	}
	return result.String()
}

// Store implements the Place interface.
type Store struct {
}

// Accept implements a store visit.
func (s *Store) Accept(v Visitor) string {
	return v.VisitStore(s)
}

// BuySmt implements a store visit.
func (s *Store) BuySmt() string {
	return "bought some beer"
}

// Cinema implements the Place interface.
type Cinema struct {
}

// Accept implements a cinema visit.
func (c *Cinema) Accept(v Visitor) string {
	return v.VisitCinema(c)
}

// WatchFilm implements a cinema visit.
func (c *Cinema) WatchFilm() string {
	return "watched Lost Highway"
}

// Theater implements the Place interface.
type Theater struct {
}

// Accept implements a theater visit.
func (t *Theater) Accept(v Visitor) string {
	return v.VisitTheater(t)
}

// WatchPlay implements a theater visit.
func (t *Theater) WatchPlay() string {
	return "watched Eugene Onegin"
}
