package pattern

import (
	"testing"
)

func TestBuilder(t *testing.T) {
	expect := "Elden Ring - FromSoftware - Souls-like"
	builder := NewBuilder().SetName("Elden Ring").SetCompany("FromSoftware").SetGenre("Souls-like")
	res := builder.Build().GetString()

	if res != expect {
		t.Errorf("Expect result to equal %s, but got %s.\n", expect, res)
	}
}

func TestDirector(t *testing.T) {
	expect := "Elden Ring - FromSoftware - Souls-like"
	builder := NewBuilder().SetName("Elden Ring").SetCompany("FromSoftware").SetGenre("Souls-like")
	director := NewDirector(builder)
	res := director.BuildGOY().GetString()

	if res != expect {
		t.Errorf("Expected result should be equal %s, but got %s.\n", expect, res)
	}
}

func TestSetterDirector(t *testing.T) {
	expect := "Stray - BlueTwelve Studio - Cyberpunk"
	builderL := NewBuilder().SetName("Elden Ring").SetCompany("FromSoftware").SetGenre("Souls-like")

	director := NewDirector(builderL)

	builderR := NewBuilder().SetName("Stray").SetCompany("BlueTwelve Studio").SetGenre("Cyberpunk")
	director.SetBuilder(builderR)
	res := director.BuildGOY().GetString()

	if res != expect {
		t.Errorf("Expected result should be equal %s, but got %s.\n", expect, res)
	}
}

func TestChain(t *testing.T) {
	fileA := FileA{Next: FileB{Next: FileC{}}}

	result := fileA.GetAccess(2)
	expect := "Owner has access to the fileA"

	if result != expect {
		t.Errorf("Expected result should be equal: %s, but got: %s.\n", expect, result)
	}
}

func TestCommand(t *testing.T) {
	computer0 := Computer{}

	invoker := Invoker{}

	invoker.Insert(&OnCommand{device: &computer0})
	invoker.Insert(&OffCommand{device: &computer0})
	invoker.Insert(&OnCommand{device: &computer0})
	invoker.Insert(&OnCommand{device: &computer0})
	invoker.ExecuteAll()

	expect := true
	result := computer0.IsRunning()

	if result != expect {
		t.Errorf("Expected result should be equal: %t, but got: %t.\n", expect, result)
	}
}

func TestFacade(t *testing.T) {
	expect := "Aggressively defends territories.\nCreates a burrow.\nCreates Golang.\n"
	gopher := NewGopher01()
	result := gopher.DoYourTasks()

	if result != expect {
		t.Errorf("Expected result should be equal:\n%s, but got:\n%s.\n", expect, result)
	}
}

func TestFactory(t *testing.T) {
	factory := Creater{}
	gopher := factory.Create("gopher")

	result := gopher.Greet()
	expect := "Hello world!"

	if result != expect {
		t.Errorf("Expected result should be equal: %s, but got: %s.\n", expect, result)
	}
}

func TestState(t *testing.T) {
	notifyEn := NotifyEn{}
	webAlert := WebAlert{}
	webAlert.SetState(&notifyEn)

	result := webAlert.Notify()
	expect := "Something went wrong"

	if result != expect {
		t.Errorf("Expected result should be equal: %s, but got: %s.\n", expect, result)
	}
}

func TestStrategy(t *testing.T) {
	search := Context{strategy: &GoogleSearch{}}

	result := search.Search("hunger")
	expect := "Google found solution for hunger"

	if result != expect {
		t.Errorf("Expected result should be equal: %s, but got: %s.\n", expect, result)
	}
}

func TestSetStrategy(t *testing.T) {
	search := Context{strategy: &GoogleSearch{}}
	search.SetStrategy(&BingSearch{})

	result := search.Search("hunger")
	expect := "Bing found solution for hunger"

	if result != expect {
		t.Errorf("Expected result should be equal: %s, but got: %s.\n", expect, result)
	}
}

func TestVisitor(t *testing.T) {
	city := new(City)
	city.Add(&Store{})
	city.Add(&Theater{})
	city.Add(&Cinema{})

	result := city.Accept(&Human{name: "Andrey"})
	expect := "Andrey bought some beer\nAndrey watched Eugene Onegin\nAndrey watched Lost Highway\n"

	if result != expect {
		t.Errorf("Expected result should be equal:\n%s, but:\n%s\n", expect, result)
	}
}
