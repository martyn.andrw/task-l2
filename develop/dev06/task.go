package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

/*
=== Утилита cut ===

Принимает STDIN, разбивает по разделителю (TAB) на колонки, выводит запрошенные

Поддержать флаги:
-f - "fields" - выбрать поля (колонки)
-d - "delimiter" - использовать другой разделитель
-s - "separated" - только строки с разделителем

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type config struct {
	path string

	fFlag arrayInt
	dFlag string
	sFlag bool
}

// arrayInt implements flag.Value interface
type arrayInt struct {
	array [][]int
}

// Set sets value to arrayInt
func (a *arrayInt) Set(value string) error {
	for _, fields := range strings.Split(value, ",") {
		arr := []int{}
		for _, field := range strings.Split(fields, "-") {
			num, err := strconv.Atoi(field)
			if err != nil {
				return err
			}
			arr = append(arr, num)
		}
		(*a).array = append((*a).array, arr)
	}
	return nil
}

// String returns a string created from an arrayInt
func (a *arrayInt) String() string {
	var str strings.Builder
	str.WriteString("fields:[")
	for i, arr := range a.array {
		if i != 0 {
			str.WriteRune(',')
		}
		for j, el := range arr {
			if j != 0 {
				str.WriteRune('-')
			}
			str.WriteString(strconv.Itoa(el))
		}
	}
	str.WriteRune(']')
	return str.String()
}

// Get returns a pointer to array
func (a *arrayInt) Get() *[][]int {
	return &(*a).array
}

// Contains returns true if val is in intervals
func (a *arrayInt) Contains(val int) bool {
	for _, arr := range (*a).array {
		if len(arr) == 1 && val == arr[0] {
			return true
		}
		if len(arr) == 2 && val >= arr[0] && val < arr[1] {
			return true
		}
	}
	return false
}

var cfg config

func init() {
	cfg = config{}
	flag.Var(&cfg.fFlag, "f", "выбрать поля (колонки)")
	flag.StringVar(&cfg.dFlag, "d", "\t", "использовать другой разделитель")
	flag.StringVar(&cfg.path, "path", "", "путь к файлу")
	flag.BoolVar(&cfg.sFlag, "s", false, "только строки с разделителем")
}

func cut(strs string) string {
	var sb strings.Builder

	if cfg.sFlag && !strings.Contains(strs, cfg.dFlag) {
		return sb.String()
	}

	for i, str := range strings.Split(strs, cfg.dFlag) {
		if cfg.fFlag.Contains(i + 1) {
			if i != 0 {
				sb.WriteString(cfg.dFlag)
			}
			sb.WriteString(str)
		}
	}

	return sb.String()
}

func main() {
	flag.Parse()
	if cfg.path != "" {
		file, err := os.Open(cfg.path)
		if err != nil {
			log.Fatalf("Error when opening file: %s", err)
		}
		defer file.Close()

		var str string
		fileScanner := bufio.NewScanner(file)
		for fileScanner.Scan() {
			str = fileScanner.Text()

			res := cut(str)
			if res != "" {
				fmt.Println("result: ", res)
			}
		}
		if err := fileScanner.Err(); err != nil {
			log.Fatalf("Error while reading file: %s", err)
		}
	} else {
		for {
			reader := bufio.NewReader(os.Stdin)
			fmt.Print("Enter text: ")
			str, _ := reader.ReadString('\n')
			if strings.Contains(str, `\q`) {
				break
			}

			res := cut(str)
			if res != "" {
				fmt.Println("result: ", res)
			}
		}
	}
}
