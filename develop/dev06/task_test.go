package main

import (
	"testing"
)

func TestCut0(t *testing.T) {
	str := "Hello world!"
	cfg.dFlag = " "
	cfg.fFlag.Set("1")

	exp := "Hello"
	res := cut(str)
	if res != exp {
		t.Errorf("Expected value: %s, but got: %s", exp, res)
	}
}

func TestCut1(t *testing.T) {
	str := "Hello world! I am a new gopher"
	cfg.dFlag = " "
	cfg.fFlag.Set("1-5,7")

	exp := "Hello world! I am gopher"
	res := cut(str)
	if res != exp {
		t.Errorf("Expected value: %s, but got: %s", exp, res)
	}
}

func TestCut2(t *testing.T) {
	str := "Helloworld!"
	cfg.dFlag = " "
	cfg.sFlag = true
	cfg.fFlag.Set("1")

	exp := ""
	res := cut(str)
	if res != exp {
		t.Errorf("Expected value: %s, but got: %s", exp, res)
	}
}
