package main

import (
	"errors"
	"fmt"
	"strings"
	"unicode"
)

/*
=== Задача на распаковку ===

Создать Go функцию, осуществляющую примитивную распаковку строки, содержащую повторяющиеся символы / руны, например:
	- "a4bc2d5e" => "aaaabccddddde"
	- "abcd" => "abcd"
	- "45" => "" (некорректная строка)
	- "" => ""
Дополнительное задание: поддержка escape - последовательностей
	- qwe\4\5 => qwe45 (*)
	- qwe\45 => qwe44444 (*)
	- qwe\\5 => qwe\\\\\ (*)

В случае если была передана некорректная строка функция должна возвращать ошибку. Написать unit-тесты.

Функция должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

// UnpackString unpacks string
func UnpackString(packedString string) (string, error) {
	var unpacked strings.Builder
	packedRunes := []rune(packedString)

	if len(packedRunes) == 0 {
		return unpacked.String(), nil
	}
	if unicode.IsDigit(packedRunes[0]) {
		return unpacked.String(), errors.New("incorrect string")
	}

	packedRunes = append(packedRunes, ' ')

	var buf rune
	slash := false
	wasNum := true
	num := 0

	for _, r := range packedRunes {
		if unicode.IsDigit(r) && !slash {
			num *= 10
			num += int(r) - 48
			wasNum = true
			continue
		}
		if r == '\\' && !slash {
			slash = true
			continue
		}

		for ; num > 0; num-- {
			unpacked.WriteRune(buf)
		}

		if !wasNum {
			unpacked.WriteRune(buf)
		} else {
			wasNum = false
		}

		buf = r
		slash = false
	}

	return unpacked.String(), nil
}

func main() {
	var str string
	fmt.Print("Insert your unpacked string: ")
	fmt.Scan(&str)

	fmt.Println(UnpackString(str))
}
