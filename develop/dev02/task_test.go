package main

import (
	"testing"
)

func TestUnpackNulStr(t *testing.T) {
	str := ""
	exp := ""

	res, err := UnpackString(str)
	if err != nil {
		t.Errorf("Expected value: <%s>, but got an error: <%s>", exp, err)
	}

	if res != exp {
		t.Errorf("Expected value: <%s>, but got: <%s>", exp, res)
	}
}

func TestUnpackNormalStr0(t *testing.T) {
	str := "abcd"
	exp := "abcd"

	res, err := UnpackString(str)
	if err != nil {
		t.Errorf("Expected value: <%s>, but got an error: <%s>", exp, err)
	}

	if res != exp {
		t.Errorf("Expected value: <%s>, but got: <%s>", exp, res)
	}
}

func TestUnpackNormalStr1(t *testing.T) {
	str := "a2bc10d"
	exp := "aabccccccccccd"

	res, err := UnpackString(str)
	if err != nil {
		t.Errorf("Expected value: <%s>, but got an error: <%s>", exp, err)
	}

	if res != exp {
		t.Errorf("Expected value: <%s>, but got: <%s>", exp, res)
	}
}

func TestUnpackIncorrect(t *testing.T) {
	str := "45"

	res, err := UnpackString(str)
	if err == nil {
		t.Errorf("Expected non-nil error and zero-value, but got err: <%s>, value: <%s>", err, res)
	}
}

func TestUnpackZero(t *testing.T) {
	str := "ab0cd0"
	exp := "ac"

	res, err := UnpackString(str)
	if err != nil {
		t.Errorf("Expected value: <%s>, but got an error: <%s>", exp, err)
	}

	if res != exp {
		t.Errorf("Expected value: <%s>, but got: <%s>", exp, res)
	}
}

func TestUnpackEscStr1(t *testing.T) {
	str := "a\\22"
	exp := "a22"

	res, err := UnpackString(str)
	if err != nil {
		t.Errorf("Expected value: <%s>, but got an error: <%s>", exp, err)
	}

	if res != exp {
		t.Errorf("Expected value: <%s>, but got: <%s>", exp, res)
	}
}

func TestUnpackEscStr2(t *testing.T) {
	str := "qwe\\\\5"
	exp := "qwe\\\\\\\\\\"

	res, err := UnpackString(str)
	if err != nil {
		t.Errorf("Expected value: <%s>, but got an error: <%s>", exp, err)
	}

	if res != exp {
		t.Errorf("Expected value: <%s>, but got: <%s>", exp, res)
	}
}

func TestUnpackEscStr3(t *testing.T) {
	str := "qwe\\45"
	exp := "qwe44444"

	res, err := UnpackString(str)
	if err != nil {
		t.Errorf("Expected value: <%s>, but got an error: <%s>", exp, err)
	}

	if res != exp {
		t.Errorf("Expected value: <%s>, but got: <%s>", exp, res)
	}
}

func TestUnpackEscStr4(t *testing.T) {
	str := "qwe\\4\\5"
	exp := "qwe45"

	res, err := UnpackString(str)
	if err != nil {
		t.Errorf("Expected value: <%s>, but got an error: <%s>", exp, err)
	}

	if res != exp {
		t.Errorf("Expected value: <%s>, but got: <%s>", exp, res)
	}
}
