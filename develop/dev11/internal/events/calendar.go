package events

import "errors"

// EventInterface provides interface for events
type EventInterface interface {
	Date() string
	User() string
	Title() string
	Description() string
	Time() string
	ID() string
}

// AddEvent adds new event to calendar
func (c *Calendar) AddEvent(event EventInterface) error {
	if ok, err := isDataCorrect(event.Date()); !ok {
		return err
	}

	id := event.ID()
	if id == "" {
		id = getID(event)
	}

	c.mu.Lock()

	if _, exists := c.repository[event.User()]; !exists {
		c.repository[event.User()] = make(map[string]Event)
	}
	if _, exists := c.repository[event.User()][id]; exists {
		return errors.New("event exists")
	}

	c.repository[event.User()][id] = Event{
		user:        event.User(),
		date:        event.Date(),
		title:       event.Title(),
		description: event.Description(),
		time:        event.Time(),
		id:          id,
	}

	c.mu.Unlock()
	return nil
}

// DeleteEvent deletes event from calendar
func (c *Calendar) DeleteEvent(user, id string) bool {
	c.mu.Lock()
	defer c.mu.Unlock()

	if _, exists := c.repository[user]; !exists {
		return false
	}
	if _, exists := c.repository[user][id]; !exists {
		return false
	}

	delete(c.repository[user], id)
	return true
}

// UpdateEvent updates event in calendar
func (c *Calendar) UpdateEvent(newEvent EventInterface) (bool, error) {
	c.mu.Lock()
	defer c.mu.Unlock()

	if _, exists := c.repository[newEvent.User()]; !exists {
		return false, errors.New("user not exists")
	}
	if _, exists := c.repository[newEvent.User()][newEvent.ID()]; !exists {
		return false, errors.New("event not exists")
	}

	c.repository[newEvent.User()][newEvent.ID()] = Event{
		user:        newEvent.User(),
		date:        newEvent.Date(),
		title:       newEvent.Title(),
		description: newEvent.Description(),
		time:        newEvent.Time(),
		id:          newEvent.ID(),
	}

	return true, nil
}
