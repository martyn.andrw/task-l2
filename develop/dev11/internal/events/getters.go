package events

// GetEventsDay returns all user events for the day
func (c *Calendar) GetEventsDay(user, date string) []Event {
	res := []Event{}

	c.mu.RLock()
	events, exists := c.repository[user]
	c.mu.RUnlock()

	if !exists {
		return res
	}

	for _, event := range events {
		if event.date == date {
			res = append(res, event)
		}
	}
	return res
}

// GetEventsWeek returns all user events for the week
func (c *Calendar) GetEventsWeek(user, date string) ([]Event, error) {
	res := []Event{}
	var err error

	for i := 0; i < 7; i++ {
		res = append(res, c.GetEventsDay(user, date)...)
		date, err = increaseDay(date)
		if err != nil {
			return nil, err
		}
	}
	return res, nil
}

// GetEventsMonth returns all user events for the month
func (c *Calendar) GetEventsMonth(user, date string) ([]Event, error) {
	res := []Event{}
	var err error

	for i := 0; i < 30; i++ {
		res = append(res, c.GetEventsDay(user, date)...)
		date, err = increaseDay(date)
		if err != nil {
			return nil, err
		}
	}
	return res, nil
}
