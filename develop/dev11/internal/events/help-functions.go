package events

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"
)

var (
	count = 0
	mu    sync.Mutex
)

func isDataCorrect(date string) (bool, error) {
	correct := "2022-12-31"
	if len(date) < len(correct) {
		return false, errors.New("incorrect length")
	}

	ds := date[len(date)-2:]
	ms := date[len(date)-5 : len(date)-3]
	ys := date[len(date)-10 : len(date)-6]

	di, err := strconv.Atoi(ds)
	if err != nil {
		return false, err
	}

	mi, err := strconv.Atoi(ms)
	if err != nil {
		return false, err
	}

	_, err = strconv.Atoi(ys)
	if err != nil {
		return false, err
	}

	if di < 0 || di > 31 || mi > 12 || mi < 1 {
		return false, fmt.Errorf("date <%s> is wrong", date)
	}

	return true, nil
}

func increaseDay(d string) (string, error) {
	ds := d[len(d)-2:]
	ms := d[len(d)-5 : len(d)-3]
	ys := d[len(d)-10 : len(d)-6]

	di, err := strconv.Atoi(ds)
	if err != nil {
		return "", err
	}

	mi, err := strconv.Atoi(ms)
	if err != nil {
		return "", err
	}

	yi, err := strconv.Atoi(ys)
	if err != nil {
		return "", err
	}

	if di < 0 || di > 31 || mi > 12 || mi < 1 {
		return "", fmt.Errorf("date <%s> is wrong", d)
	}

	di++
	if mi == 2 {
		if yi%4 == 0 && di > 29 ||
			yi%4 != 0 && di > 28 {
			di = 1
			mi++
		}
	}

	if (mi == 1 || mi == 3 ||
		mi == 5 || mi == 7 ||
		mi == 8 || mi == 10) &&
		di > 31 || (mi == 4 ||
		mi == 6 || mi == 9 ||
		mi == 11) && di > 30 {
		di = 1
		mi++
	}

	if mi == 12 && di > 31 {
		di = 1
		mi = 1
		yi++
	}

	ms = strconv.Itoa(mi)
	if len(ms) < 2 {
		ms = "0" + ms
	}
	ds = strconv.Itoa(di)
	if len(ds) < 2 {
		ds = "0" + ds
	}
	return fmt.Sprintf("%s-%s-%s", strconv.Itoa(yi), ms, ds), nil
}

func getID(event EventInterface) string {
	var sb strings.Builder
	sb.WriteString(event.Date())
	sb.WriteString("_")
	sb.WriteString(event.Time())
	sb.WriteString("_")

	mu.Lock()
	sb.WriteString(strconv.Itoa(count))
	count++
	mu.Unlock()

	return sb.String()
}
