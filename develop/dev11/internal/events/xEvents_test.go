package events

import (
	"testing"
)

func TestHelp_incDay(t *testing.T) {
	date := "2022-09-09"
	exp := "2022-09-10"

	date, err := increaseDay(date)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}

	if date != exp {
		t.Errorf("Expected value: %s, but got: %s", exp, date)
	}
}

func TestHelp_incWeek(t *testing.T) {
	date := "2022-09-09"
	exp := "2022-09-16"

	var err error
	for i := 0; i < 7; i++ {
		date, err = increaseDay(date)
		if err != nil {
			t.Errorf("Got an error: %s", err)
		}
	}

	if date != exp {
		t.Errorf("Expected value: %s, but got: %s", exp, date)
	}
}

func TestHelp_incMonth(t *testing.T) {
	date := "2022-09-09"
	exp := "2022-10-09"

	var err error
	for i := 0; i < 30; i++ {
		date, err = increaseDay(date)
		if err != nil {
			t.Errorf("Got an error: %s", err)
		}
	}

	if date != exp {
		t.Errorf("Expected value: %s, but got: %s", exp, date)
	}
}

func TestCal_addEvent(t *testing.T) {
	expectedID := "2022-08-07_20:05_0"
	event := Event{
		user:        "Andrey",
		date:        "2022-08-07",
		title:       "Something",
		description: "Something happened",
		time:        "20:05",
	}
	cal := NewCalendar()
	err := cal.AddEvent(event)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}

	event.id = expectedID

	if _, exist := cal.repository["Andrey"]; !exist {
		t.Errorf("User not added")
	}

	if _, exist := cal.repository["Andrey"][expectedID]; !exist {
		t.Errorf("Event not added")
	}

	if cal.repository["Andrey"][expectedID] != event {
		t.Errorf("Expected value: %v, but got %v.", event, cal.repository["Andrey"][expectedID])
	}
}

func TestCal_delEvent(t *testing.T) {
	expectedID := "2022-08-07_20:05_0"
	user := "Andrey"

	event := Event{
		user:        user,
		date:        "2022-08-07",
		title:       "Something",
		description: "Something happened",
		time:        "20:05",
		id:          expectedID,
	}
	cal := NewCalendar()
	err := cal.AddEvent(event)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}

	ok := cal.DeleteEvent(user, expectedID)
	if !ok {
		t.Errorf("Event not deleted")
	}

	if _, exist := cal.repository["Andrey"]; !exist {
		t.Errorf("User not added")
	}

	if _, exist := cal.repository["Andrey"][expectedID]; exist {
		t.Errorf("Event not deleted")
	}
}

func TestCal_updEvent(t *testing.T) {
	expectedID := "2022-08-07_20:05_0"

	event := Event{
		user:        "Andrey",
		date:        "2022-08-07",
		title:       "Something",
		description: "Something happened",
		time:        "20:05",
		id:          expectedID,
	}
	cal := NewCalendar()
	err := cal.AddEvent(event)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}

	event.id = expectedID
	event.title = "Nothing"
	event.date = "2022-09-10"

	cal.UpdateEvent(event)

	if _, exist := cal.repository["Andrey"]; !exist {
		t.Errorf("User not added")
	}

	if _, exist := cal.repository["Andrey"][expectedID]; !exist {
		t.Errorf("Event not added or deleted")
	}

	if cal.repository["Andrey"][expectedID] != event {
		t.Errorf("Expected value: %v, but got %v.", event, cal.repository["Andrey"][expectedID])
	}
}

func TestCal_getDayEvent(t *testing.T) {
	expectedID0 := "2022-08-07_20:05_0"
	expectedID1 := "2022-08-07_20:05_1"
	expectedID2 := "2022-08-07_20:05_2"

	event0 := Event{
		user:        "Andrey",
		date:        "2022-08-07",
		title:       "Something",
		description: "Something happened",
		time:        "20:05",
		id:          expectedID0,
	}
	event1 := Event{
		user:        "Not Andrey",
		date:        "2022-08-12",
		title:       "Something",
		description: "Something happened",
		time:        "20:05",
		id:          expectedID1,
	}
	event2 := Event{
		user:        "Andrey",
		date:        "2022-08-07",
		title:       "Something",
		description: "Something happened",
		time:        "22:05",
		id:          expectedID2,
	}

	cal := NewCalendar()

	err := cal.AddEvent(event0)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}
	err = cal.AddEvent(event1)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}
	err = cal.AddEvent(event2)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}

	events := cal.GetEventsDay("Andrey", "2022-08-07")
	if len(events) != 2 {
		t.Errorf("Wrong length")
		return
	}

	exp := []Event{event0, event2}

	if events[0] != cal.repository["Andrey"][expectedID0] ||
		events[1] != cal.repository["Andrey"][expectedID2] {
		t.Errorf("Expected value: \n%v\nbut got:\n%v", exp, events)
	}
}

func TestCal_getWeekEvent(t *testing.T) {
	expectedID0 := "test_0"
	expectedID1 := "test_1"
	expectedID2 := "test_2"

	event0 := Event{
		user:        "Andrey",
		date:        "2022-08-07",
		title:       "Something",
		description: "Something happened",
		time:        "20:05",
		id:          expectedID0,
	}
	event1 := Event{
		user:        "Not Andrey",
		date:        "2022-08-12",
		title:       "Something",
		description: "Something happened",
		time:        "20:05",
		id:          expectedID1,
	}
	event2 := Event{
		user:        "Andrey",
		date:        "2022-08-13",
		title:       "Something",
		description: "Something happened",
		time:        "22:05",
		id:          expectedID2,
	}

	cal := NewCalendar()

	err := cal.AddEvent(event0)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}
	err = cal.AddEvent(event1)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}
	err = cal.AddEvent(event2)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}

	events, err := cal.GetEventsWeek("Andrey", "2022-08-07")
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}
	if len(events) != 2 {
		t.Errorf("Wrong length")
		return
	}

	exp := []Event{event0, event2}

	if events[0] != cal.repository["Andrey"][expectedID0] ||
		events[1] != cal.repository["Andrey"][expectedID2] {
		t.Errorf("Expected value: \n%v\nbut got:\n%v", exp, events)
	}
}

func TestCal_getMonthEvent(t *testing.T) {
	expectedID0 := "test_0"
	expectedID1 := "test_1"
	expectedID2 := "test_2"

	event0 := Event{
		user:        "Andrey",
		date:        "2022-08-20",
		title:       "Something",
		description: "Something happened",
		time:        "20:05",
		id:          expectedID0,
	}
	event1 := Event{
		user:        "Andrey",
		date:        "2022-11-12",
		title:       "Something",
		description: "Something happened",
		time:        "20:05",
		id:          expectedID1,
	}
	event2 := Event{
		user:        "Andrey",
		date:        "2022-09-02",
		title:       "Something",
		description: "Something happened",
		time:        "22:05",
		id:          expectedID2,
	}

	cal := NewCalendar()

	err := cal.AddEvent(event0)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}
	err = cal.AddEvent(event1)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}
	err = cal.AddEvent(event2)
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}

	events, err := cal.GetEventsMonth("Andrey", "2022-08-20")
	if err != nil {
		t.Errorf("Got an error: %s", err)
	}
	if len(events) != 2 {
		t.Errorf("Wrong length")
		return
	}

	exp := []Event{event0, event2}

	if events[0] != cal.repository["Andrey"][expectedID0] ||
		events[1] != cal.repository["Andrey"][expectedID2] {
		t.Errorf("Expected value: \n%v\nbut got:\n%v", exp, events)
	}
}
