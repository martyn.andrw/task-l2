package events

import (
	"sync"
)

// Event is a structure for event which will be added to Calendar
type Event struct {
	user        string
	date        string
	title       string
	description string
	time        string
	id          string
}

// Calendar is structure for calendar
type Calendar struct {
	mu         sync.RWMutex
	repository map[string]map[string]Event
}

// NewCalendar returns new Calendar
func NewCalendar() *Calendar {
	return &Calendar{repository: make(map[string]map[string]Event)}
}

// User returns user from event
func (e Event) User() string {
	return e.user
}

// Date returns date from event
func (e Event) Date() string {
	return e.date
}

// Title returns title from event
func (e Event) Title() string {
	return e.title
}

// Description returns description from event
func (e Event) Description() string {
	return e.description
}

// Time returns time from event
func (e Event) Time() string {
	return e.time
}

// ID returns id from event
func (e Event) ID() string {
	return e.id
}
