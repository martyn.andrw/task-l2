package server

import (
	"errors"
	"net/http"

	"gitlab.com/martyn.andrw/develop/dev11/internal/events"
)

// Implementation ...
type Implementation struct {
	logg LoggerInterface
	cal  *events.Calendar
	app  *Application
}

// NewImplementation returns new Implementation
func NewImplementation(logger LoggerInterface, calendar *events.Calendar) *Implementation {
	res := Implementation{logg: logger, cal: calendar, app: NewApp()}
	res.app.mux.HandleFunc("/create", res.Middleware(res.Create))
	res.app.mux.HandleFunc("/delete", res.Middleware(res.Delete))
	res.app.mux.HandleFunc("/update", res.Middleware(res.Update))
	res.app.mux.HandleFunc("/events_for_day", res.Middleware(res.Day))
	res.app.mux.HandleFunc("/events_for_week", res.Middleware(res.Week))
	res.app.mux.HandleFunc("/events_for_month", res.Middleware(res.Month))
	return &res
}

// Middleware is intermediate layer for logging
func (i *Implementation) Middleware(fun http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		id := r.URL.Query()["id"]
		if len(id) < 1 || id[0] == "" {
			i.logg.WriteError(errors.New("no <id> in request"))
			http.Error(w, "No id in request", http.StatusBadRequest)
		}

		date := r.URL.Query()["date"]
		if len(date) < 1 || date[0] == "" {
			i.logg.WriteError(errors.New("no <date> in request"))
			http.Error(w, "No date in request", http.StatusBadRequest)
		}

		fun(w, r)
	}
}

// StartApp starts application
func (i *Implementation) StartApp() {
	err := http.ListenAndServe(i.app.cfg.Host+":"+i.app.cfg.Port, i.app.mux)
	if err != nil {
		i.logg.WriteError(err)
	}
}
