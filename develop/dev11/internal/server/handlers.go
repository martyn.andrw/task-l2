package server

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/martyn.andrw/develop/dev11/internal/validation"
)

func getDataFromError(err error, msg string) (data []byte) {
	m := make(map[string]string)
	m[msg] = err.Error()
	errVal := validation.Error{Err: m}
	data, _ = validation.MarshalErr(errVal)
	return
}

// Create is a handler for /create
func (i *Implementation) Create(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		w.Write(getDataFromError(err, "body request"))
		http.Error(w, "Wrong request", http.StatusBadRequest)
	}

	event, err := validation.UnMarshal(body)
	if err != nil {
		w.Write(getDataFromError(err, "validation"))
		http.Error(w, "Validation", http.StatusServiceUnavailable)
	}

	id := r.URL.Query()["id"]
	date := r.URL.Query()["date"]

	event.UserA = id[0]
	event.DateA = date[0]

	err = i.cal.AddEvent(event)
	if err != nil {
		w.Write(getDataFromError(err, "data"))
		http.Error(w, "Calendar", http.StatusServiceUnavailable)
	} else {
		w.Write(validation.OK)
	}
}

// Delete is a handler for /delete
func (i *Implementation) Delete(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		w.Write(getDataFromError(err, "body request"))
		http.Error(w, "Wrong request", http.StatusBadRequest)
	}

	event, err := validation.UnMarshal(body)
	if err != nil {
		w.Write(getDataFromError(err, "validation"))
		http.Error(w, "Validation", http.StatusServiceUnavailable)
	}

	id := r.URL.Query()["id"]
	date := r.URL.Query()["date"]

	event.UserA = id[0]
	event.DateA = date[0]

	deleted := i.cal.DeleteEvent(event.User(), event.ID())
	if deleted {
		w.Write(validation.OK)
	} else {
		w.Write(getDataFromError(errors.New("event not found"), "delete"))
		http.Error(w, "Calendar", http.StatusServiceUnavailable)
	}
	fmt.Println(i.cal)
}

// Update is a handler for /update
func (i *Implementation) Update(w http.ResponseWriter, r *http.Request) { // todo
	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		w.Write(getDataFromError(err, "body request"))
		http.Error(w, "Wrong request", http.StatusBadRequest)
	}

	event, err := validation.UnMarshal(body)
	if err != nil {
		w.Write(getDataFromError(err, "validation"))
		http.Error(w, "Validation", http.StatusServiceUnavailable)
	}

	id := r.URL.Query()["id"]
	date := r.URL.Query()["date"]

	event.UserA = id[0]
	event.DateA = date[0]

	updated, err := i.cal.UpdateEvent(event)
	if !updated {
		w.Write(getDataFromError(err, "data"))
		http.Error(w, "Calendar", http.StatusServiceUnavailable)
	} else {
		w.Write(validation.OK)
	}
}

// Day is a handler for /events_for_day
func (i *Implementation) Day(w http.ResponseWriter, r *http.Request) {
	_, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		w.Write(getDataFromError(err, "body request"))
		http.Error(w, "Wrong request", http.StatusBadRequest)
	}

	date := r.URL.Query()["date"][0]
	user := r.URL.Query()["id"][0]

	res := []map[string]string{}
	events := i.cal.GetEventsDay(user, date)

	for _, event := range events {
		m := make(map[string]string)
		m["date"] = event.Date()
		m["user"] = event.User()
		m["title"] = event.Title()
		m["description"] = event.Description()
		m["time"] = event.Time()
		m["id"] = event.ID()
		res = append(res, m)
	}

	resVal := validation.Result{Res: res}
	data, err := validation.MarshalRes(resVal)
	if err != nil {
		w.Write(getDataFromError(err, "validation"))
		http.Error(w, "Validation", http.StatusServiceUnavailable)
	} else {
		w.Write(data)
	}
}

// Week is a handler for /events_for_week
func (i *Implementation) Week(w http.ResponseWriter, r *http.Request) {
	_, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		w.Write(getDataFromError(err, "body request"))
		http.Error(w, "Wrong request", http.StatusBadRequest)
	}

	date := r.URL.Query()["date"][0]
	user := r.URL.Query()["id"][0]

	res := []map[string]string{}
	events, err := i.cal.GetEventsWeek(user, date)
	if err != nil {
		w.Write(getDataFromError(err, "week"))
		http.Error(w, "Week", http.StatusServiceUnavailable)
	}

	for _, event := range events {
		m := make(map[string]string)
		m["date"] = event.Date()
		m["user"] = event.User()
		m["title"] = event.Title()
		m["description"] = event.Description()
		m["time"] = event.Time()
		m["id"] = event.ID()
		res = append(res, m)
	}

	resVal := validation.Result{Res: res}
	data, err := validation.MarshalRes(resVal)
	if err != nil {
		w.Write(getDataFromError(err, "validation"))
		http.Error(w, "Validation", http.StatusServiceUnavailable)
	} else {
		w.Write(data)
	}
}

// Month is a handler for /events_for_month
func (i *Implementation) Month(w http.ResponseWriter, r *http.Request) {
	_, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		w.Write(getDataFromError(err, "body request"))
		http.Error(w, "Wrong request", http.StatusBadRequest)
	}

	date := r.URL.Query()["date"][0]
	user := r.URL.Query()["id"][0]

	res := []map[string]string{}
	events, err := i.cal.GetEventsMonth(user, date)
	if err != nil {
		w.Write(getDataFromError(err, "month"))
		http.Error(w, "Month", http.StatusServiceUnavailable)
	}

	for _, event := range events {
		m := make(map[string]string)
		m["date"] = event.Date()
		m["user"] = event.User()
		m["title"] = event.Title()
		m["description"] = event.Description()
		m["time"] = event.Time()
		m["id"] = event.ID()
		res = append(res, m)
	}

	resVal := validation.Result{Res: res}
	data, err := validation.MarshalRes(resVal)
	if err != nil {
		w.Write(getDataFromError(err, "validation"))
		http.Error(w, "Validation", http.StatusServiceUnavailable)
	} else {
		w.Write(data)
	}
}
