package server

import (
	"net/http"

	"gitlab.com/martyn.andrw/develop/dev11/internal/config"
)

// LoggerInterface provides interface for logger
type LoggerInterface interface {
	WriteError(error)
	WriteInfo(string)
	WriteOK(string)
}

// EventInterface provides interface for event
type EventInterface interface {
	Date() string
	User() string
	Title() string
	Description() string
	Time() string
}

// Application is a structure for web-app
type Application struct {
	mux *http.ServeMux
	cfg *config.Config
}

// NewApp returns new Application
func NewApp() *Application {
	return &Application{
		mux: http.NewServeMux(),
		cfg: config.Cfg(),
	}
}
