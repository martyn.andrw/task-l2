package config

import (
	"io/ioutil"

	yaml "gopkg.in/yaml.v3"
)

// Config has host and port
// It can be modified
type Config struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
}

var (
	cfg *Config
)

func init() {
	bytes, err := ioutil.ReadFile("config.yml")
	if err != nil {
		panic(err)
	}

	cfg = &Config{}
	err = yaml.Unmarshal(bytes, cfg)
	if err != nil {
		panic(err)
	}
}

// Cfg returns new config
func Cfg() *Config {
	return cfg
}
