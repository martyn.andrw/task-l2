package validation

import "encoding/json"

// OK is a ready response to the server when everything is in order
var OK []byte

func init() {
	OK, _ = json.Marshal(map[string]string{"result": "ok"})
}

// Result is a structure for parsing json
type Result struct {
	Res []map[string]string `json:"result"`
}

// Error is a structure for parsing json
type Error struct {
	Err map[string]string `json:"error"`
}

// UnMarshal unmarshals data to event
func UnMarshal(data []byte) (*Event, error) {
	var e Event
	err := json.Unmarshal(data, &e)
	return &e, err
}

// MarshalRes marshals data to result
func MarshalRes(res Result) ([]byte, error) {
	data, err := json.Marshal(res)
	return data, err
}

// MarshalErr marshals data to error
func MarshalErr(errJ Error) ([]byte, error) {
	data, err := json.Marshal(errJ)
	return data, err
}
