package validation

// Event is a struct for parsing json
// Also Event implements events.EventInterface
type Event struct {
	DateA        string `json:"date"`
	UserA        string `json:"user"`
	TitleA       string `json:"title"`
	DescriptionA string `json:"description"`
	TimeA        string `json:"time"`
	IDA          string `json:"id"`
}

// Date returns date from Event
func (e Event) Date() string {
	return e.DateA
}

// User returns user from Event
func (e Event) User() string {
	return e.UserA
}

// Title returns title from Event
func (e Event) Title() string {
	return e.TitleA
}

// Description returns description from Event
func (e Event) Description() string {
	return e.DescriptionA
}

// Time returns time from Event
func (e Event) Time() string {
	return e.TimeA
}

// ID returns id from Event
func (e Event) ID() string {
	return e.IDA
}
