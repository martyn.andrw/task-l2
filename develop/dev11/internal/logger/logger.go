package logger

import (
	"bufio"
	"fmt"
	"os"
	"time"
)

var (
	reset = "\033[0m"
	red   = "\033[31m"
	green = "\033[32m"
	cyan  = "\033[36m"
)

// Logger is a structure for logging
type Logger struct {
	stdOut *bufio.Writer
	stdErr *bufio.Writer
}

// NewLogger returns new Logger
func NewLogger() *Logger {
	return &Logger{
		stdOut: bufio.NewWriter(os.Stdout),
		stdErr: bufio.NewWriter(os.Stderr),
	}
}

// WriteError writes error to stdErr
func (l *Logger) WriteError(err error) { // todo writing to file
	msg := fmt.Sprintf("%s\t[%s ERR%s ]\t%s\n", time.Now().Format(time.UnixDate), red, reset, err)
	_, err1 := l.stdErr.WriteString(msg)
	if err1 != nil {
		fmt.Println(err1)
	}
	err1 = l.stdErr.Flush()
	if err1 != nil {
		fmt.Println(err1)
	}
}

// WriteInfo writes information to stdOut
func (l *Logger) WriteInfo(message string) {
	msg := fmt.Sprintf("%s\t[%sINFO%s]\t%s\n", time.Now().Format(time.UnixDate), cyan, reset, message)
	_, err := l.stdOut.WriteString(msg)
	if err != nil {
		fmt.Println(err)
	}
	err = l.stdOut.Flush()
	if err != nil {
		fmt.Println(err)
	}
}

// WriteOK writes OK status to stdOut
func (l *Logger) WriteOK(message string) {
	msg := fmt.Sprintf("%s\t[%sOK%s]\t%s\n", time.Now().Format(time.UnixDate), green, reset, message)
	_, err := l.stdOut.WriteString(msg)
	if err != nil {
		fmt.Println(err)
	}
	err = l.stdOut.Flush()
	if err != nil {
		fmt.Println(err)
	}
}
