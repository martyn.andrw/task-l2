package main

import (
	"testing"
	"time"
)

func TestOr(t *testing.T) {
	sig := func(after time.Duration) <-chan interface{} {
		c := make(chan interface{})
		go func() {
			defer close(c)
			time.Sleep(after)
		}()
		return c
	}
	timeAfter := time.Now().Add(1 * time.Second)

	<-or(
		sig(2*time.Hour),
		sig(5*time.Minute),
		sig(1*time.Second),
		sig(1*time.Hour),
		sig(1*time.Minute),
	)
	timeNow := time.Now()

	if timeNow.Format(time.UnixDate) != timeAfter.Format(time.UnixDate) {
		t.Errorf("Expected value: %s, but got: %s", timeAfter.Format(time.UnixDate), timeNow.Format(time.UnixDate))
	}
}
