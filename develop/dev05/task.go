package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

/*
=== Утилита grep ===

Реализовать утилиту фильтрации (man grep)

Поддержать флаги:
-A - "after" печатать +N строк после совпадения
-B - "before" печатать +N строк до совпадения
-C - "context" (A+B) печатать ±N строк вокруг совпадения
-c - "count" (количество строк)
-i - "ignore-case" (игнорировать регистр)
-v - "invert" (вместо совпадения, исключать)
-F - "fixed", точное совпадение со строкой, не паттерн
-n - "line num", печатать номер строки

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type config struct {
	path string
	find string

	aFlag   int
	bFlag   int
	ctxFlag int
	cFlag   int
	iFlag   bool
	vFlag   bool
	fFlag   bool
	nFlag   bool
}

var cfg config

func init() {
	cfg = config{}
	flag.IntVar(&cfg.aFlag, "A", 0, "печатать +N строк после совпадения")
	flag.IntVar(&cfg.bFlag, "B", 0, "печатать +N строк до совпадения")
	flag.IntVar(&cfg.ctxFlag, "C", 0, "печатать ±N строк вокруг совпадения")
	flag.IntVar(&cfg.cFlag, "c", 0, "количество строк")

	flag.StringVar(&cfg.path, "path", "", "путь к файлу")
	flag.StringVar(&cfg.find, "find", "", "искомая строка")

	flag.BoolVar(&cfg.iFlag, "i", false, "игнорировать регистр")
	flag.BoolVar(&cfg.vFlag, "v", false, "вместо совпадения, исключать")
	flag.BoolVar(&cfg.fFlag, "F", false, "точное совпадение со строкой, не паттерн")
	flag.BoolVar(&cfg.nFlag, "n", false, "печатать номер строки")
}

// classic grep
func grep(strs []string) []string {
	res := []string{}
	count := cfg.cFlag
	if count <= 0 {
		count = len(strs)
	}

	req := fmt.Sprintf(cfg.find)
	re, _ := regexp.Compile(req)

	for i, str := range strs {
		strC := str
		if cfg.iFlag {
			strC = strings.ToLower(str)
		}
		if (re.MatchString(strC) && !cfg.fFlag) || (strings.Contains(strC, cfg.find) && cfg.fFlag) {
			before := i - cfg.ctxFlag
			if before < 0 {
				before = 0
			}

			if cfg.bFlag > 0 {
				before = i - cfg.bFlag
				if before < 0 {
					before = 0
				}
			}

			after := i + cfg.ctxFlag + 1
			if after > len(strs) {
				after = len(strs)
			}

			if cfg.aFlag > 0 {
				after = i + cfg.aFlag + 1
				if after > len(strs) {
					after = len(strs)
				}
			}

			for ; before < after; before++ {
				var strAdd strings.Builder
				if cfg.nFlag {
					strAdd.WriteString(strconv.Itoa(before))
					if before == i {
						strAdd.WriteString(": ")
					} else {
						strAdd.WriteString("- ")
					}
				}
				strAdd.WriteString(strs[before])
				res = append(res, strAdd.String())
			}
			count--
		}
		if count == 0 {
			break
		}
	}

	return res
}

// invert grep
func invertGrep(strs []string) []string {
	res := []string{}
	count := cfg.cFlag
	if count <= 0 {
		count = len(strs)
	}

	req := fmt.Sprintf(cfg.find)
	re, _ := regexp.Compile(req)

	for i, str := range strs {
		strC := str
		if cfg.iFlag {
			strC = strings.ToLower(str)
		}
		if !((re.MatchString(strC) && !cfg.fFlag) || (strings.Contains(strC, cfg.find) && cfg.fFlag)) && count > 0 {
			var strAdd strings.Builder
			if cfg.nFlag {
				strAdd.WriteString(strconv.Itoa(i))
				strAdd.WriteString(": ")
			}
			strAdd.WriteString(str)
			res = append(res, strAdd.String())
			count--
		}
	}

	return res
}

func main() {
	flag.Parse()
	if cfg.find == "" {
		log.Fatal("-find is required")
	}
	if cfg.iFlag {
		cfg.find = strings.ToLower(cfg.find)
	}

	if cfg.path == "" {
		cfg.path = "tests/test0.txt"
	}

	file, err := os.Open(cfg.path)
	if err != nil {
		log.Fatalf("Error when opening file: %s", err)
	}
	defer file.Close()

	var strs []string

	fileScanner := bufio.NewScanner(file)
	for fileScanner.Scan() {
		strs = append(strs, fileScanner.Text())
	}
	if err := fileScanner.Err(); err != nil {
		log.Fatalf("Error while reading file: %s", err)
	}

	if cfg.vFlag {
		res := invertGrep(strs)
		for _, r := range res {
			fmt.Println(r)
		}
	} else {
		res := grep(strs)
		for _, r := range res {
			fmt.Println(r)
		}
	}
}
