package main

import (
	"bufio"
	"os"
	"testing"
)

func TestGrep1(t *testing.T) {
	cfg.path = "tests/test0.txt"

	file, err := os.Open(cfg.path)
	if err != nil {
		t.Errorf("Error when opening file: %s", err)
	}
	defer file.Close()

	var strs []string

	fileScanner := bufio.NewScanner(file)
	for fileScanner.Scan() {
		strs = append(strs, fileScanner.Text())
	}
	if err := fileScanner.Err(); err != nil {
		t.Errorf("Error while reading file: %s", err)
	}

	cfg.find = "В своём отраженьи"
	cfg.nFlag = true

	res := grep(strs)
	exp := []string{"1: В своём отраженьи узнайте Махно"}

	if len(exp) != len(res) {
		t.Errorf("Expected value: \n%v, \nbut got: \n%v.", exp, res)
	}
	for i := range res {
		if res[i] != exp[i] {
			t.Errorf("Expected value: \n%v, \nbut got: \n%v.", exp, res)
		}
	}
}

func TestGrep2(t *testing.T) {
	cfg.path = "tests/test0.txt"

	file, err := os.Open(cfg.path)
	if err != nil {
		t.Errorf("Error when opening file: %s", err)
	}
	defer file.Close()

	var strs []string

	fileScanner := bufio.NewScanner(file)
	for fileScanner.Scan() {
		strs = append(strs, fileScanner.Text())
	}
	if err := fileScanner.Err(); err != nil {
		t.Errorf("Error while reading file: %s", err)
	}

	cfg.find = "[Махно]"
	cfg.fFlag = true

	res := grep(strs)
	exp := []string{}

	if len(exp) != len(res) {
		t.Errorf("Expected value: \n%v, \nbut got: \n%v.", exp, res)
	}
	for i := range res {
		if res[i] != exp[i] {
			t.Errorf("Expected value: \n%v, \nbut got: \n%v.", exp, res)
		}
	}
}
