package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strings"
	"unicode"

	"golang.org/x/exp/slices"
)

/*
=== Утилита sort ===

Отсортировать строки (man sort)
Основное

Поддержать ключи

-k — указание колонки для сортировки
-n — сортировать по числовому значению
-r — сортировать в обратном порядке
-u — не выводить повторяющиеся строки

Дополнительное

Поддержать ключи

-M — сортировать по названию месяца
-b — игнорировать хвостовые пробелы
-c — проверять отсортированы ли данные
-h — сортировать по числовому значению с учётом суффиксов

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

type config struct {
	workDir string
	path    string

	// necessary flags
	kFlag int
	nFlag bool
	rFlag bool
	uFlag bool

	// additional flag
	mFlag bool
	bFlag bool
	cFlag bool
	hFlag bool
}

var cfg config

func init() {
	cfg = config{}
	cfg.workDir, _ = os.Getwd()
	flag.IntVar(&cfg.kFlag, "k", 1, "указание колонки для сортировки")

	flag.BoolVar(&cfg.nFlag, "n", false, "сортировать по числовому значению")
	flag.BoolVar(&cfg.rFlag, "r", false, "сортировать в обратном порядке")
	flag.BoolVar(&cfg.uFlag, "u", false, "не выводить повторяющиеся строки")

	flag.StringVar(&cfg.path, "path", "", "путь к файлу")

	flag.BoolVar(&cfg.mFlag, "M", false, "сортировать по названию месяца")
	flag.BoolVar(&cfg.bFlag, "b", false, "игнорировать хвостовые пробелы")
	flag.BoolVar(&cfg.cFlag, "c", false, "проверить отсортированы ли данные")
	flag.BoolVar(&cfg.hFlag, "h", false, "сортировать по числовому значению с учётом суффиксов") // didn't understand + https://t.me/c/1166594172/2452
}

func toNum(str string) int {
	num := 0
	for _, s := range str {
		if unicode.IsDigit(s) {
			num *= 10
			num += int(s) - 48
		}
	}
	return num
}

func isRighter(left, right []string) bool {
	if len(left) < cfg.kFlag {
		return cfg.rFlag
	}
	if len(right) < cfg.kFlag {
		return !cfg.rFlag
	}

	if cfg.hFlag {
		if cfg.rFlag {
			return len(left[cfg.kFlag-1]) < len(right[cfg.kFlag-1])
		}
		return len(left[cfg.kFlag-1]) > len(right[cfg.kFlag-1])
	}

	if cfg.nFlag {
		if cfg.rFlag {
			return toNum(left[cfg.kFlag-1]) < toNum(right[cfg.kFlag-1])
		}
		return toNum(left[cfg.kFlag-1]) > toNum(right[cfg.kFlag-1])
	}

	if cfg.mFlag {
		if unicode.IsDigit(rune(left[cfg.kFlag-1][0])) {
			if cfg.rFlag {
				return toNum(left[cfg.kFlag-1]) < toNum(right[cfg.kFlag-1])
			}
			return toNum(left[cfg.kFlag-1]) > toNum(right[cfg.kFlag-1])
		}
		if cfg.rFlag {
			return left[cfg.kFlag-1] < right[cfg.kFlag-1]
		}
		return left[cfg.kFlag-1] > right[cfg.kFlag-1]
	}

	if cfg.rFlag {
		return left[cfg.kFlag-1] < right[cfg.kFlag-1]
	}
	return left[cfg.kFlag-1] > right[cfg.kFlag-1]
}

func removeDuplicatesIfNeeded(strs [][]string) [][]string {
	if !cfg.uFlag {
		return strs
	}

	res := make([][]string, 0, len(strs))
	prev := strs[0]
	res = append(res, prev)

	for _, act := range strs {
		if len(act) != len(prev) {
			res = append(res, act)
			continue
		}
		for i := range act {
			if act[i] != prev[i] {
				res = append(res, act)
				break
			}
		}
		prev = act
	}

	return res
}

// SortSliceStr returns sorted slice
func SortSliceStr(input [][]string) [][]string {
	strs := slices.Clone(input)

	for i := range strs {
		for j := range strs[i+1:] {
			if isRighter(strs[i], strs[i+j+1]) {
				strs[i], strs[i+j+1] = strs[i+j+1], strs[i]
			}
		}
	}
	return removeDuplicatesIfNeeded(strs)
}

func isSorted(strs [][]string) ([]string, bool) {
	prev := strs[0]
	for _, act := range strs {
		if isRighter(prev, act) {
			return act, false
		}
		prev = act
	}
	return nil, true
}

func main() {
	flag.Parse()
	file, err := os.Open(cfg.path)
	if err != nil {
		log.Fatalf("Error when opening file: %s", err)
	}
	defer file.Close()

	var strs [][]string

	fileScanner := bufio.NewScanner(file)
	for fileScanner.Scan() {
		if cfg.bFlag {
			strs = append(strs, strings.Split(strings.TrimSpace(fileScanner.Text()), " "))
		} else {
			strs = append(strs, strings.Split(fileScanner.Text(), " "))
		}
	}
	if err := fileScanner.Err(); err != nil {
		log.Fatalf("Error while reading file: %s", err)
	}

	if cfg.cFlag {
		errStr, ok := isSorted(strs)
		if !ok {
			fmt.Println("gosort: tests/test0.txt:2: неправильный порядок: ", errStr)
		}
	} else {
		strs = SortSliceStr(strs)
		for _, str := range strs {
			fmt.Println(str)
		}
	}
}
