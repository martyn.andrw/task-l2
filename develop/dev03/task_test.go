package main

import (
	"bufio"
	"os"
	"strings"
	"testing"
)

func getStr(t *testing.T, path string) [][]string {
	file, err := os.Open(path)
	if err != nil {
		t.Errorf("Error when opening file: %s", err)
	}
	defer file.Close()

	var strs [][]string

	fileScanner := bufio.NewScanner(file)
	for fileScanner.Scan() {
		if cfg.bFlag {
			strs = append(strs, strings.Split(strings.TrimSpace(fileScanner.Text()), " "))
		} else {
			strs = append(strs, strings.Split(fileScanner.Text(), " "))
		}
	}
	if err := fileScanner.Err(); err != nil {
		t.Errorf("Error while reading file: %s", err)
	}

	return strs
}

func TestSort1(t *testing.T) {
	path := "tests/test0.txt"
	strs := SortSliceStr(getStr(t, path))
	errStr, ok := isSorted(strs)
	if !ok {
		t.Errorf("gosort: %s:2: неправильный порядок: %s\n", path, errStr)
		for _, str := range strs {
			t.Errorf("\t%v\n", str)
		}
	}
}

func TestSort2(t *testing.T) {
	cfg = config{kFlag: 1, rFlag: true}
	path := "tests/test1.txt"
	strs := SortSliceStr(getStr(t, path))
	strs = SortSliceStr(strs)
	errStr, ok := isSorted(strs)
	if !ok {
		t.Errorf("gosort: %s:2: неправильный порядок: %s\n", path, errStr)
		for _, str := range strs {
			t.Errorf("\t%v\n", str)
		}
	}
}

func TestSort3(t *testing.T) {
	path := "tests/test2.txt"
	cfg = config{uFlag: true, kFlag: 1}
	strs := SortSliceStr(getStr(t, path))
	strs = SortSliceStr(strs)
	errStr, ok := isSorted(strs)
	if !ok {
		t.Errorf("gosort: %s:2: неправильный порядок: %s\n", path, errStr)
		for _, str := range strs {
			t.Errorf("\t%v\n", str)
		}
	}
}

func TestSort4(t *testing.T) {
	path := "tests/test3.txt"
	cfg = config{mFlag: true, kFlag: 1}
	strs := SortSliceStr(getStr(t, path))
	strs = SortSliceStr(strs)
	errStr, ok := isSorted(strs)
	if !ok {
		t.Errorf("gosort: %s:2: неправильный порядок: %s\n", path, errStr)
		for _, str := range strs {
			t.Errorf("\t%v\n", str)
		}
	}
}
