package main

import (
	"fmt"
	"sort"
	"strings"

	"golang.org/x/exp/slices"
)

/*
=== Поиск анаграмм по словарю ===

Напишите функцию поиска всех множеств анаграмм по словарю.
Например:
'пятак', 'пятка' и 'тяпка' - принадлежат одному множеству,
'листок', 'слиток' и 'столик' - другому.

Входные данные для функции: ссылка на массив - каждый элемент которого - слово на русском языке в кодировке utf8.
Выходные данные: Ссылка на мапу множеств анаграмм.
Ключ - первое встретившееся в словаре слово из множества
Значение - ссылка на массив, каждый элемент которого, слово из множества. Массив должен быть отсортирован по возрастанию.
Множества из одного элемента не должны попасть в результат.
Все слова должны быть приведены к нижнему регистру.
В результате каждое слово должно встречаться только один раз.

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

func sortString(w string) string {
	s := strings.Split(w, "")
	sort.Strings(s)
	return strings.Join(s, "")
}

func isAnagram(left, right string) bool {
	lSort := sortString(strings.ToLower(strings.Join(strings.Fields(left), "")))
	rSort := sortString(strings.ToLower(strings.Join(strings.Fields(right), "")))
	return lSort == rSort
}

// FindGroupAnagrams returns anagram groups
func FindGroupAnagrams(words *[]string) *map[string]*[]string {
	mapAnagram := make(map[string]*[]string)

	if len(*words) < 1 {
		return &mapAnagram
	}

mainLoop:
	for i, word := range *words {
		if _, exists := mapAnagram[word]; exists {
			continue mainLoop
		}

		for _, w := range (*words)[:i] {
			if w != word && isAnagram(word, w) {
				if !slices.Contains(*mapAnagram[w], word) {
					*mapAnagram[w] = append(*mapAnagram[w], word)
				}
				continue mainLoop
			}
		}
		mapAnagram[word] = new([]string)
		*mapAnagram[word] = append(*mapAnagram[word], word)
	}

	for key := range mapAnagram {
		if len(*mapAnagram[key]) == 1 {
			delete(mapAnagram, key)
			continue
		}
		slices.Sort(*mapAnagram[key])
	}

	return &mapAnagram
}

func main() {
	words := []string{"пятак", "пятка", "тяпка", "листок", "слиток", "столик", "суслик"}
	m := FindGroupAnagrams(&words)
	fmt.Print("map[")
	for key, val := range *m {
		fmt.Printf("%s : %v ", key, val)
	}
	fmt.Print("]\n")
}
