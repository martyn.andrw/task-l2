package main

import (
	"fmt"
	"strings"
	"testing"
)

func writeError(t *testing.T, exp *map[string][]string, res *map[string]*[]string) {
	var sb strings.Builder

	sb.WriteString(fmt.Sprintf("Expected value: <%v>, but got: <", exp))
	sb.WriteString("map[")
	for key, val := range *res {
		sb.WriteString(fmt.Sprintf("%s:%v", key, *val))
	}
	sb.WriteString("]>\n")
	t.Error(sb.String())
}

func TestAnagram(t *testing.T) {
	words := []string{"пятак", "тяпка", "пятка", "листок", "слиток", "столик", "столик", "суслик"}

	result := FindGroupAnagrams(&words)
	expect := map[string][]string{
		"листок": {"листок", "слиток", "столик"},
		"пятак":  {"пятак", "пятка", "тяпка"},
	}

	if len(*result) != len(expect) {
		writeError(t, &expect, result)
		return
	}
	for key, value := range expect {
		val, exist := (*result)[key]
		if !exist || len(value) != len(*val) {
			writeError(t, &expect, result)
			return
		}
		for i := 0; i < len(value); i++ {
			if value[i] != (*val)[i] {
				writeError(t, &expect, result)
				return
			}
		}
	}
}
