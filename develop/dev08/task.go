package main

import (
	"bufio"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"strings"
	"time"

	gps "github.com/mitchellh/go-ps"
)

/*
=== Взаимодействие с ОС ===

Необходимо реализовать свой собственный UNIX-шелл-утилиту с поддержкой ряда простейших команд:

- cd <args> - смена директории (в качестве аргумента могут быть то-то и то)
- pwd - показать путь до текущего каталога
- echo <args> - вывод аргумента в STDOUT
- kill <args> - "убить" процесс, переданный в качесте аргумента (пример: такой-то пример)
- ps - выводит общую информацию по запущенным процессам в формате *такой-то формат*


Так же требуется поддерживать функционал fork/exec-команд

Дополнительно необходимо поддерживать конвейер на пайпах (linux pipes, пример cmd1 | cmd2 | .... | cmdN).

*Шелл — это обычная консольная программа, которая будучи запущенной, в интерактивном сеансе выводит некое приглашение
в STDOUT и ожидает ввода пользователя через STDIN. Дождавшись ввода, обрабатывает команду согласно своей логике
и при необходимости выводит результат на экран. Интерактивный сеанс поддерживается до тех пор, пока не будет введена команда выхода (например \quit).

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

var pwdVar string

func init() {
	pwdVar, _ = os.Getwd()
}

func cd(args ...string) (string, error) {
	runes := []rune(args[0])
	path := args[0]

	if path == ".." {
		path = pwdVar[:strings.LastIndex(pwdVar, "/")]
		if path == "" {
			path = "/"
		}
		fmt.Println(path)
	} else if runes[0] != '/' {
		if pwdVar[len(pwdVar)-1] != '/' {
			path = pwdVar + "/" + path
		} else {
			path = pwdVar + path
		}
	}

	dir, err := os.Stat(path)
	if !os.IsNotExist(err) {
		if dir.IsDir() {
			pwdVar = path
		}
	}
	return "", err
}

func pwd(args ...string) (string, error) {
	return pwdVar, nil
}

func echo(args ...string) (string, error) {
	var sb strings.Builder
	sb.WriteString("echo ")
	for _, arg := range args {
		sb.WriteString(arg)
		sb.WriteString(" ")
	}
	out, err := exec.Command("sh", "-c", sb.String()).Output()
	return string(out), err
}

func kill(args ...string) (string, error) {
	var sb strings.Builder
	sb.WriteString("kill ")
	for _, arg := range args {
		sb.WriteString(arg)
		sb.WriteString(" ")
	}
	out, err := exec.Command("sh", "-c", sb.String()).Output()
	return string(out), err
}

func ps(args ...string) (string, error) {
	processList, err := gps.Processes()
	if err != nil {
		return "", err
	}

	var sb strings.Builder
	for _, process := range processList {
		sb.WriteString(
			fmt.Sprintf("%s:\t%d\t%s\n",
				time.Now().Format(time.UnixDate),
				process.Pid(),
				process.Executable(),
			))
	}
	return sb.String(), nil
}

func execCommand(f func(...string) (string, error), args ...string) {
	res, err := f(args...)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Something went wrong: %s\n", err)
	} else {
		fmt.Printf("%s\n", res)
	}
}

func main() {
	for {
		reader := bufio.NewReader(os.Stdin)
		us, _ := user.Current()
		fmt.Printf("%s:%s$ ", us.Name, pwdVar)
		str, _ := reader.ReadString('\n')

		strs := strings.Split(str[:len(str)-1], " ")
		switch strs[0] {
		case `\q`, `\quit`:
			return
		case `cd`:
			execCommand(cd, strs[1:]...)
		case `pwd`:
			execCommand(pwd, strs[1:]...)
		case `echo`:
			execCommand(echo, strs[1:]...)
		case `kill`:
			execCommand(kill, strs[1:]...)
		case `ps`:
			execCommand(ps, strs[1:]...)
		default:
			fmt.Fprintf(os.Stderr, "Command '%s' not found.\n", strs[0])
		}
	}
}
