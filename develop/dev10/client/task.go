package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/martyn.andrw/develop/dev10/pkg/duration"
	"gitlab.com/martyn.andrw/develop/dev10/pkg/telnet"
)

/*
=== Утилита telnet ===

Реализовать примитивный telnet клиент:
Примеры вызовов:
go-telnet --timeout=10s host port
go-telnet mysite.ru 8080
go-telnet --timeout=3s 1.1.1.1 123

Программа должна подключаться к указанному хосту (ip или доменное имя) и порту по протоколу TCP.
После подключения STDIN программы должен записываться в сокет, а данные полученные и сокета должны выводиться в STDOUT
Опционально в программу можно передать таймаут на подключение к серверу (через аргумент --timeout, по умолчанию 10s).

При нажатии Ctrl+D программа должна закрывать сокет и завершаться. Если сокет закрывается со стороны сервера, программа должна также завершаться.
При подключении к несуществующему сервер, программа должна завершаться через timeout.
*/

var (
	timeout duration.Duration
	port    string
	host    string
)

func init() {
	flag.Var(&timeout, "timeout", "время ожидания подключения. (используйте окончания s, ms, etc)")
	flag.StringVar(&port, "port", "3000", "port подключения")
	flag.StringVar(&host, "host", "localhost", "host подключения")
}

func main() {
	flag.Parse()

	if timeout.Timeout() == time.Duration(0) {
		timeout.SetTimeout(time.Second * time.Duration(10))
	}

	chSign := make(chan os.Signal, 1)
	signal.Notify(chSign, syscall.SIGQUIT)

	t := telnet.NewClient(timeout.Timeout(), host, port)

	err := t.Connect()
	if err != nil {
		log.Fatal(err)
	}
	err = t.Writer(chSign)
	if err != nil {
		log.Fatal(err)
	}
}
