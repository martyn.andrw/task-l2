package server

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

const (
	port = "3000"
	host = "localhost"
)

func ListenAndServe(msgChan chan<- string) {
	listener, err := net.Listen("tcp", fmt.Sprintf("%s:%s", host, port))
	if err != nil {
		log.Fatal(err)
	}

	defer listener.Close()

	for {
		fmt.Println("Waiting for connection...")
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal(err)
		}

		fmt.Println("New connection. Waiting for messages...")
		go func(conn net.Conn) {
			defer conn.Close()

			fmt.Printf("Serving new conn %s\n", conn.RemoteAddr())
			connReader := bufio.NewReader(conn)

			for {
				message, err := connReader.ReadString('\n')
				if err != nil {
					if err == io.EOF {
						fmt.Printf("Connection %s closed.\n", conn.RemoteAddr())
						break
					}
					fmt.Fprintf(os.Stderr, "error reading from conn: %s\n", err)
					break
				}
				message = strings.TrimSpace(message)

				fmt.Printf("From: %s Received: %s\n", conn.RemoteAddr(), string(message))
				msgChan <- message

				_, err = conn.Write([]byte(message + "\n"))
				if err != nil {
					fmt.Fprintf(os.Stderr, "error writing to conn: %v\n", err)
					break
				}
			}

			fmt.Printf("Done serving client %s\n", conn.RemoteAddr())
		}(conn)
	}
}
