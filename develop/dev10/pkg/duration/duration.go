package duration

import (
	"strconv"
	"strings"
	"time"
)

// Duration implements flag.Value interface
type Duration struct {
	td time.Duration
}

// Set transforms string to time.Duration
func (d *Duration) Set(value string) error {
	var val string

	switch {
	case strings.Contains(value, "ns"):
		val = value[:strings.Index(value, "ns")]
		d.td = time.Nanosecond
	case strings.Contains(value, "ms"):
		val = value[:strings.Index(value, "ms")]
		d.td = time.Millisecond
	case strings.Contains(value, "s"):
		val = value[:strings.Index(value, "s")]
		d.td = time.Second
	case strings.Contains(value, "m"):
		val = value[:strings.Index(value, "m")]
		d.td = time.Minute
	case strings.Contains(value, "h"):
		val = value[:strings.Index(value, "h")]
		d.td = time.Hour
	default:
		val = "10"
		d.td = time.Second
	}

	timeout, err := strconv.Atoi(val)
	if err != nil {
		return err
	}
	d.td *= time.Duration(timeout)
	return nil
}

// String returns a string representing the duration in the form "72h3m0.5s"
func (d *Duration) String() string {
	return d.td.String()
}

// Timeout returns td as time.Duration
func (d *Duration) Timeout() time.Duration {
	return d.td
}

// SetTimeout sets new value to td
func (d *Duration) SetTimeout(timeout time.Duration) {
	d.td = timeout
}
