package duration

import (
	"testing"
	"time"
)

func TestSet(t *testing.T) {
	to := Duration{}
	to.Set("10s")

	exp := time.Second * time.Duration(10)

	if to.td != exp {
		t.Errorf("Expected value: %v, but got: %v", exp, to.td)
	}
}

func TestString(t *testing.T) {
	to := Duration{}
	to.Set("10s")

	res := to.String()
	exp := "10s"

	if res != exp {
		t.Errorf("Expected value: %s, but got: %s", exp, to.td)
	}
}

func TestTimeOut(t *testing.T) {
	to := Duration{}
	to.Set("10s")

	res := to.Timeout()
	exp := to.td

	if res != exp {
		t.Errorf("Expected value: %v, but got: %v", exp, to.td)
	}
}

func TestSetTimeOut(t *testing.T) {
	to := Duration{}
	to.Set("10s")

	to.SetTimeout(time.Millisecond)

	res := to.td
	exp := time.Millisecond

	if res != exp {
		t.Errorf("Expected value: %v, but got: %v", exp, to.td)
	}
}
