package telnet

import (
	"testing"
	"time"
)

func TestTimeout(t *testing.T) {
	client := NewClient(time.Second, "localhost", "3000")
	start := time.Now()

	client.Connect()

	if start.Add(time.Second).Format(time.UnixDate) != time.Now().Format(time.UnixDate) {
		t.Errorf("Expected timeout about 1s, but got: %v", time.Since(start))
	}
}
