package telnet

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"net"
	"os"
	"strings"
	"time"
)

// Client is the object that will be used for communication betweet client and server
type Client struct {
	conn    net.Conn
	timeout time.Duration
	host    string
	port    string
}

// Connect connects to server
func (t *Client) Connect() (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), t.timeout)
	defer cancel()

	t.conn, err = connect(ctx, t.host, t.port)
	return
}

func connect(ctx context.Context, host, port string) (conn net.Conn, err error) {
	for {
		select {
		case <-ctx.Done():
			return
		default:
			conn, err = net.Dial("tcp", fmt.Sprintf("%s:%s", host, port))
			time.Sleep(100 * time.Millisecond) // not so fast
			if err == nil {
				return
			}
		}
	}
}

// Writer gets messages from STDIN and sends them to the server
func (t *Client) Writer(quit <-chan os.Signal) error {
	console := bufio.NewReader(os.Stdin)
	connReader := bufio.NewReader(t.conn)

	defer t.conn.Close()

	for {
		select {
		case <-quit:
			return nil
		default:
			fmt.Print("Message to send: ")
			data, err := console.ReadString('\n')
			if err == io.EOF {
				fmt.Print("\nClosing connection\n")
			}
			if err != nil {
				fmt.Fprintf(os.Stderr, "error reading string: %v\n", err)
				return err
			}
			text := strings.TrimSpace(data)

			fmt.Fprintf(t.conn, text+"\n")
			if text == "exit" {
				fmt.Print("\nClosing connection\n")
				return err
			}

			message, err := connReader.ReadString('\n')
			if err != nil {
				if err == io.EOF {
					fmt.Println("Connection closed.")
					return err
				}
				fmt.Fprintf(os.Stderr, "error reading from conn: %v\n", err)
				return err
			}
			message = strings.TrimSpace(message)
			fmt.Printf("Echo from server: %s\n", message)
		}
	}
}

// NewClient is a constructor for Telnet.Client
func NewClient(timeout time.Duration, host, port string) Client {
	return Client{
		timeout: timeout,
		host:    host,
		port:    port,
	}
}
