package main

import (
	"gitlab.com/martyn.andrw/develop/dev10/pkg/server"
)

func main() {
	msgChan := make(chan string, 1)
	go server.ListenAndServe(msgChan)
	for {
		<-msgChan
	}
}
