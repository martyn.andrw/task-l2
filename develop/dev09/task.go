package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"

	"github.com/opesun/goquery"
)

/*
=== Утилита wget ===

Реализовать утилиту wget с возможностью скачивать сайты целиком

Программа должна проходить все тесты. Код должен проходить проверки go vet и golint.
*/

var dir string
var url string
var easyMod bool

func init() {
	flag.StringVar(&url, "url", "http://example.com", "download site link")
	flag.StringVar(&dir, "dir", "", "save folder")
	flag.BoolVar(&easyMod, "easyMod", false, "using exec.Command")
}

func easyWget(url string) {
	output, err := exec.Command("sh", "-c", fmt.Sprintf("wget -r -k -l 1 -p -E -nc %s", url)).Output()
	if err != nil {
		log.Fatal(err)
	}
	log.Println(output)
}

func complicatedWget(url string) {
	dir = dirParse(url)
	getRes(url, ".html")

	x, _ := goquery.ParseUrl(url)
	for _, url := range x.Find("").Attrs("href") {
		if strings.Contains(url, ".png") || strings.Contains(url, ".jpg") ||
			strings.Contains(url, ".css") || strings.Contains(url, ".js") {
			getRes(url, "")
		}
	}
}

func nameParse(url, extension string) string {
	urls := strings.Split(url, "/")
	filename := urls[len(urls)-1]

	if !strings.Contains(filename, extension) {
		filename += extension
	}

	return filename
}

func dirParse(url string) string {
	if dir == "" {
		urls := strings.Split(url, "/")
		return urls[len(urls)-1]
	}
	return dir
}

func getRes(url string, extension string) {
	resp, err := http.Get(url)
	if err != nil {
		log.Fatalln(err)
	}
	defer resp.Body.Close()

	fileName := nameParse(url, extension)

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err = os.Mkdir(dir, 0777)
		if err != nil {
			log.Fatal(err)
		}
	}

	file, err := os.Create(dir + "/" + fileName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	flag.Parse()

	if valid, err := regexp.MatchString("^(http|https)://", url); valid && err == nil {
		if easyMod {
			easyWget(url)
		} else {
			complicatedWget(url)
		}
	} else {
		log.Fatal("invalid url")
	}
}
