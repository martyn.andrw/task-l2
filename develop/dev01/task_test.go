package main

import (
	"testing"
	"time"
)

func TestNtpTime(t *testing.T) {
	expect := time.Now().Format(time.UnixDate)
	result, err := NtpTime("0.beevik-ntp.pool.ntp.org")
	if err != nil {
		t.Errorf("Expected result should be equal: %s, but got error: %s.\n", expect, err)
	}

	if result != expect {
		t.Errorf("Expected result should be equal: %s, but got: %s.\n", expect, result)
	}
}
